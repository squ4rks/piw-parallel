import rq
import redis

from tasks import get_config

queue = rq.Queue(connection=redis.Redis())

router = {
    'device_type': 'cisco_ios',
    'host': 'sandbox-iosxe-latest-1.cisco.com',
    'port': 22,
    'username': 'developer',
    'password': 'C1sco12345',
    'secret': 'C1sco12345'
}
routers = [router]

for r in routers:
    job = queue.enqueue(get_config, device=r)
    print(job.id)