def f():
    print(f"Beginning of function")
    yield
    print(f"End of function")

try:
    y = f()
    next(y)
    print("Control back in main")
    next(y)
except StopIteration as e:
    pass
