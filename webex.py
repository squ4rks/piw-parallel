import asyncio
import aiohttp

import os

token = os.environ.get("WEBEX_TOKEN")

async def do_req():
    async with aiohttp.ClientSession() as session:
        session.headers.update({
            'Authorization': f"Bearer {token}"
        })

        rooms_url = "https://webexapis.com/v1/rooms"

        async with session.get(rooms_url) as resp:
            rooms = await resp.json()

            num_direct = 0
            num_group = 0

            for r in rooms['items']:
                if r['type'] == "direct":
                    num_direct += 1
                else:
                    num_group += 1
            print(f"Direct: {num_direct}")
            print(f"Group: {num_group}")

asyncio.run(do_req())