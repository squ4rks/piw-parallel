import rq
import redis

job_id = input("Job Id: ")
job = rq.job.Job(job_id, connection=redis.Redis())

print(f"Status: {job.get_status()}")
print(f"Origin: {job.origin}")
print(f"Result: {job.result}")