import netmiko

def get_config(device):
    with netmiko.ConnectHandler(**device) as conn:
        out = conn.send_command("show running-config")
        return {'conf': out}