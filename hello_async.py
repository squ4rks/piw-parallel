import asyncio

async def hello_async():
    await asyncio.sleep(1)
    print("Hello async")

loop = asyncio.get_event_loop()
loop.run_until_complete(hello_async())